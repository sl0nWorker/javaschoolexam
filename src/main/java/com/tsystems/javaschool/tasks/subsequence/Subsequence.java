package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) throw new IllegalArgumentException();

        int i = 0;
        int afterIdx = 0;
        int j = 0;
        int coincidence = 0;

        for (i = 0; i < x.size(); i++) {
            if (coincidence < i) return false;
            for (j = afterIdx; j < y.size(); j++) {
                if (y.get(j).equals(x.get(i))) {
                    if (j >= afterIdx) {
                        coincidence++;
                        afterIdx = j;
                        break;
                    }
                }
            }
        }
        return coincidence == x.size() ? true : false;
    }
}
