package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.size() > 255) {
            throw new CannotBuildPyramidException();
        } else {
            if (inputNumbers.contains(null)) {
                throw new CannotBuildPyramidException();
            } else {
                Collections.sort(inputNumbers);
            }
        }

        int size = 0;
        int layers = 0;
        boolean build = false;
        int growth = 1;
        int height;
        int width;

        for (int x : inputNumbers) {
            size = size + growth++;
            layers++;
            if (size == inputNumbers.size()) {
                build = true;
                break;
            }
        }

        int[][] piramidArr;

        if (build == true) {
            height = layers;
            width = layers * 2 - 1;
            int i = inputNumbers.size() - 1;
            piramidArr = new int[height][width];
            for (int y = height - 1; y >= 0; y--) {
                int offset = (height - 1) - y;
                int cornerZeros = offset;
                for (int z = width - 1; z >= 0; z--) {
                    if ((z == width - 1 - offset) && (z >= cornerZeros)) {
                        piramidArr[y][z] = inputNumbers.get(i--);
                        offset += 2;
                    }
                }
            }
            return piramidArr;
        } else {
            throw new CannotBuildPyramidException();
        }
    }
}
